package main

import (
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/wanginator/internal/commands"
	"gitlab.com/yakshaving.art/wanginator/internal/config"
	"gitlab.com/yakshaving.art/wanginator/internal/gitlab"
	"gitlab.com/yakshaving.art/wanginator/internal/version"

	"gitlab.com/yakshaving.art/wanginator/pkg/logger"
)

func main() {
	args, err := config.ParseArgsAndEnv()
	logger.SetupLogger(args.Debug)
	if args.Version {
		logrus.Printf(version.GetVersion())
		os.Exit(0)
	}

	if err != nil {
		logrus.Fatal(err)
	}

	cfg, err := config.Load(args.ConfigFile)
	if err != nil {
		logrus.Fatal(err)
	}

	client := gitlab.New(gitlab.Args{
		Token:   args.GitlabToken,
		BaseURL: args.GitlabBaseURL,
		DryRun:  args.DryRun,
	})

	exitCode := 0
	for _, cmd := range commands.FromConfig(cfg, args.Keep) {
		if err := cmd.Execute(client); err != nil {
			if args.CarryOn {
				logrus.Errorf("command failed with error %s", err)
				exitCode = 1
			} else {
				logrus.Fatalf("command failed wirh error %s", err)
			}
		}
	}

	os.Exit(exitCode)
}
