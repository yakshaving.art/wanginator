package config

import "os"

import "flag"

import "fmt"

// Args are the arguments that can be used in this binary.
type Args struct {
	ConfigFile string
	Keep       int

	GitlabToken   string
	GitlabBaseURL string

	Debug   bool
	DryRun  bool
	Version bool
	CarryOn bool
}

// ParseArgsAndEnv parses the commandline arguments together with the
// environment variables, validates and returns an Args object, or an error if
// things are wrong
func ParseArgsAndEnv() (Args, error) {
	a := Args{
		GitlabToken:   os.Getenv("GITLAB_TOKEN"),
		GitlabBaseURL: os.Getenv("GITLAB_BASEURL"),
	}

	flag.StringVar(&a.ConfigFile, "config", "wanginator.yml", "configuration file")
	flag.IntVar(&a.Keep, "keep", 2, "how many tokens to keep after inserting a new one, 0 means all")
	flag.BoolVar(&a.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&a.DryRun, "dryrun", false, "enable dryrun mode, which prevents from applying any change")
	flag.BoolVar(&a.Version, "version", false, "show version and exit")
	flag.BoolVar(&a.CarryOn, "carry-on", false, "if set, errors are treated as warnings and execution continues")

	flag.Parse()

	if a.GitlabToken == "" {
		return a, fmt.Errorf("GITLAB_TOKEN is a mandatory environment variable")
	}
	if a.GitlabBaseURL == "" {
		return a, fmt.Errorf("GITLAB_BASEURL is a mandatory environment variable")
	}
	return a, nil
}
