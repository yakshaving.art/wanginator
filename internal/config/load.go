package config

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/yakshaving.art/wanginator/internal"
	"gopkg.in/yaml.v2"
)

// Load loads the configuration file into a Config object and returns it, or an
// error.
func Load(filename string) (internal.Config, error) {
	c := internal.Config{}

	f, err := ioutil.ReadFile(filename)
	if err != nil {
		return c, fmt.Errorf("failed to read file %s: %s", filename, err)
	}

	if err := yaml.UnmarshalStrict(f, &c); err != nil {
		return c, fmt.Errorf("failed to parse configuration file %s: %s", filename, err)
	}

	return c, nil
}
