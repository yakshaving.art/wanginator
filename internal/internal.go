package internal

import "time"

// Config the configuration itself
type Config struct {
	Tokens []TokenVar `yaml:"tokens"`
}

// TokenVar an impersonation token configuration
type TokenVar struct {
	Name     string   `yaml:"name"`
	Username string   `yaml:"username"`
	Scope    string   `yaml:"scope"`
	Varname  string   `yaml:"varname"`
	Groups   []string `yaml:"groups"`
	Projects []string `yaml:"projects"`
	Keep     *int     `yaml:"keep"`
}

// Client is a client to the Gitlab API
type Client interface {
	// We will have to fetch and cache the users by username to get the numeric ID
	CreateToken(username, name, scope string) (string, error)
	RevokeToken(username string, tokenID int) error

	ListTokens(username string) ([]Token, error)

	SetGroupVar(group, name, value string) error
	SetProjectVar(project, name, value string) error
}

// Token is a token identifier with it's creation date
type Token struct {
	ID           int
	CreationDate time.Time
}
