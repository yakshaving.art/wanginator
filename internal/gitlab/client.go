package gitlab

import (
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/yakshaving.art/wanginator/internal"
)

// Args are the arguments necessary to create a gitlab client
type Args struct {
	Token   string
	BaseURL string

	DryRun bool
}

// New returns a new configured gitlab client
func New(args Args) internal.Client {
	client, err := gitlab.NewClient(args.Token, gitlab.WithBaseURL(args.BaseURL))
	if err != nil {
		logrus.Fatalf("failed to create a gitlab client: '%s'", err)
	}

	r := newReadClient(client)

	if args.DryRun {
		return dryrunClient{c: client, ReadClient: r}
	}
	return WriteClient{c: client, ReadClient: r}
}

func s(str string) *string {
	return &str
}
