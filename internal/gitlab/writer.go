package gitlab

import (
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

// WriteClient is a gitlab client that can actually write stuff to the API
type WriteClient struct {
	c *gitlab.Client

	*ReadClient
}

// CreateToken implements internal.Client.CreateToken
func (g WriteClient) CreateToken(username, name, scope string) (string, error) {
	userID, err := g.GetUserID(username)
	if err != nil {
		return "", fmt.Errorf("could not find user %s: %s", username, err)
	}
	scopes := strings.Split(scope, ",")
	nextYear := time.Now().Add(time.Hour * time.Duration(24) * time.Duration(365))

	token, resp, err := g.c.Users.CreateImpersonationToken(userID, &gitlab.CreateImpersonationTokenOptions{
		Name:      s(name),
		Scopes:    &scopes,
		ExpiresAt: &nextYear,
	})
	if err != nil {
		return "", fmt.Errorf("failed to create impersonation token for user %s: %s", username, err)
	}
	if resp.StatusCode != 201 {
		return "", fmt.Errorf("received unexpected status code %d while creating impersonation token for %s",
			resp.StatusCode, username)
	}
	logrus.Printf("created impersonation token %s for user %s", name, username)
	logrus.Debugf("  token created at %s", token.CreatedAt.Format(time.RFC3339))
	return token.Token, nil
}

// RevokeToken implements internal.Client.RevokeToken
func (g WriteClient) RevokeToken(username string, tokenID int) error {
	userID, err := g.GetUserID(username)
	if err != nil {
		return fmt.Errorf("could not find user %s: %s", username, err)
	}
	resp, err := g.c.Users.RevokeImpersonationToken(userID, tokenID)
	if err != nil {
		return fmt.Errorf("failed to revoke token for user %s: %s", username, err)
	}
	if resp.StatusCode != 204 {
		return fmt.Errorf("received unexpected status code %d while revoking token for %s",
			resp.StatusCode, username)
	}
	logrus.Printf("revoked impersonation token for user %s", username)
	logrus.Debugf("  token revoked as an ID of %d", tokenID)
	return nil
}

// SetGroupVar implements internal.Client.SetGroupVar
func (g WriteClient) SetGroupVar(group, name, value string) error {
	_, resp, err := g.c.GroupVariables.GetVariable(group, name)

	logrus.Debugf("got response %d for get group variable %s/%s", resp.StatusCode, group, name)
	if resp.StatusCode == 404 {
		logrus.Debugf("creating group variable %s/%s", group, name)
		_, _, err = g.c.GroupVariables.CreateVariable(group, &gitlab.CreateGroupVariableOptions{
			Key:   s(name),
			Value: s(value),
		})
	} else {
		logrus.Debugf("updating group variable %s/%s", group, name)
		_, _, err = g.c.GroupVariables.UpdateVariable(group, name, &gitlab.UpdateGroupVariableOptions{
			Value: s(value),
		})
	}
	if err != nil {
		return fmt.Errorf("failed to create or update group variable %s/%s: %s", group, name, err)
	}
	logrus.Printf("configured group %s var %s", group, name)
	return nil
}

// SetProjectVar sets a project variable
func (g WriteClient) SetProjectVar(project, name, value string) error {
	_, resp, err := g.c.ProjectVariables.GetVariable(project, name, &gitlab.GetProjectVariableOptions{})
	if err != nil {
		return fmt.Errorf("failed to get project variable %s/%s: %s", project, name, err)
	}

	logrus.Debugf("got response %d for get project variable %s/%s", resp.StatusCode, project, name)
	if resp.StatusCode == 404 {
		logrus.Debugf("creating project variable %s/%s", project, name)
		_, _, err = g.c.ProjectVariables.CreateVariable(project, &gitlab.CreateProjectVariableOptions{
			Key:   s(name),
			Value: s(value),
		})
	} else {
		logrus.Debugf("updating project variable %s/%s", project, name)
		_, _, err = g.c.ProjectVariables.UpdateVariable(project, name, &gitlab.UpdateProjectVariableOptions{
			Value: s(value),
		})
	}
	if err != nil {
		return fmt.Errorf("failed to create or update project variable %s/%s: %s", project, name, err)
	}
	logrus.Printf("configured project %s var %s", project, name)
	return nil

}
