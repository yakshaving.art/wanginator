package gitlab

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/yakshaving.art/wanginator/internal"
)

// ReadClient provides read methods
type ReadClient struct {
	c *gitlab.Client

	users map[string]int
}

func newReadClient(c *gitlab.Client) *ReadClient {
	return &ReadClient{
		c:     c,
		users: make(map[string]int, 0),
	}
}

// GetUserID returns a userID for a given username
func (g *ReadClient) GetUserID(username string) (int, error) {
	if userID, ok := g.users[username]; ok {
		logrus.Debugf("  fetched user %s from cache", username)
		return userID, nil
	}

	u, _, err := g.c.Users.ListUsers(&gitlab.ListUsersOptions{Username: s(username)})
	if err != nil {
		return 0, fmt.Errorf("error while listing users by username %s: %s", username, err)
	}

	if len(u) == 0 {
		return 0, fmt.Errorf("user %s does not exists", username)
	}

	if len(u) > 1 {
		return 0, fmt.Errorf("there are %d users matching %s (WTF?)", len(u), username)
	}

	logrus.Debugf("  fetched user %s from gitlab instance", username)
	userID := u[0].ID
	g.users[username] = userID
	return userID, nil
}

// ListTokens implements internal.Client.ListTokens
func (g *ReadClient) ListTokens(username string) ([]internal.Token, error) {
	userID, err := g.GetUserID(username)
	if err != nil {
		return nil, fmt.Errorf("could not find user %s: %s", username, err)
	}

	logrus.Debugf("listing tokens for user %s", username)
	tokens := make([]internal.Token, 0)
	page := 1
	for {
		fetchedTokens, resp, err := g.c.Users.GetAllImpersonationTokens(userID, &gitlab.GetAllImpersonationTokensOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
			State: s("active"),
		})
		if err != nil {
			return nil, fmt.Errorf("failed to fetch user %s active tokens: %s", username, err)
		}

		for _, t := range fetchedTokens {
			tokens = append(tokens, internal.Token{ID: t.ID, CreationDate: *t.CreatedAt})
			logrus.Debugf("  fetched token %d %s (created on %s)", t.ID, t.Name, t.CreatedAt.Format(time.RFC3339))
		}

		if page == resp.TotalPages {
			break
		}
		page++
	}

	logrus.Debugf("%d user %s tokens found", len(tokens), username)
	return tokens, nil
}
