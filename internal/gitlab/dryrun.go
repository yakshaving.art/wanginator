package gitlab

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/yakshaving.art/wanginator/pkg/random"
)

type dryrunClient struct {
	c *gitlab.Client

	*ReadClient
}

// CreateToken implements internal.Client.CreateToken
func (g dryrunClient) CreateToken(username, name, scope string) (string, error) {
	logrus.Printf("creating %s token %s with scope %s", username, name, scope)
	return fmt.Sprintf("fake_token_%s", random.Word(12)), nil
}

// RevokeToken implements internal.Client.RevokeToken
func (g dryrunClient) RevokeToken(username string, tokenID int) error {
	logrus.Printf("revoking %s token %d", username, tokenID)
	return nil
}

// SetGroupVar implements internal.Client.SetGroupVar
func (g dryrunClient) SetGroupVar(group, name, value string) error {
	logrus.Printf("setting group var %s/%s to value %s", group, name, value)
	return nil
}

// SetProjectVar implements internal.Client.SetProjectVar
func (g dryrunClient) SetProjectVar(project, name, value string) error {
	logrus.Printf("setting project var %s/%s to value %s", project, name, value)
	return nil
}
