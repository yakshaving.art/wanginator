package version_test

import (
	"testing"

	"gitlab.com/yakshaving.art/wanginator/internal/version"
)

func TestVersionString(t *testing.T) {
	version.Commit = "mycommit"
	version.Date = "today"
	version.Version = "0.0.1"

	expected := "wanginator Version: 0.0.1 Commit: mycommit Date: today"
	if version.GetVersion() != expected {
		t.Fatalf("invalid version %s expected %s", version.GetVersion(), expected)
	}
}
