package commands

import (
	"fmt"
	"sort"

	"github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/wanginator/internal"
)

// Command is the interface for command executions
type Command interface {
	Execute(internal.Client) error
}

// CreateAndRegisterToken is a command that creates a token
type CreateAndRegisterToken struct {
	Username string
	Name     string
	Scope    string

	Groups   []string
	Projects []string
	VarName  string
}

// Execute implements the Command interface
func (c CreateAndRegisterToken) Execute(g internal.Client) error {
	token, err := g.CreateToken(c.Username, c.Name, c.Scope)
	if err != nil {
		return fmt.Errorf("failed to create a new token: %s", err)
	}

	for _, group := range c.Groups {
		if err := g.SetGroupVar(group, c.VarName, token); err != nil {
			return fmt.Errorf("failed to register group %s var %s: %s", group, c.VarName, err)
		}
	}

	for _, project := range c.Projects {
		if err := g.SetProjectVar(project, c.VarName, token); err != nil {
			return fmt.Errorf("failed to register project %s var %s: %s", project, c.VarName, err)
		}
	}

	return nil
}

// RevokeTokens command that revokes old tokens
type RevokeTokens struct {
	Username string
	Keep     int
}

// Execute implements the Command interface
func (c RevokeTokens) Execute(g internal.Client) error {
	if c.Keep == 0 {
		logrus.Debugf("Skipping tokens revocation for %s", c.Username)
		return nil
	}

	tokens, err := g.ListTokens(c.Username)
	if err != nil {
		return fmt.Errorf("failed to list tokens: %s", err)
	}

	sort.Slice(tokens, func(i int, j int) bool {
		return tokens[i].CreationDate.Before(tokens[j].CreationDate)
	})

	for i := 0; i < len(tokens)-c.Keep; i++ {
		if err := g.RevokeToken(c.Username, tokens[i].ID); err != nil {
			return fmt.Errorf("failed to revoke token for user %s: %s", c.Username, err)
		}
	}

	return nil
}
