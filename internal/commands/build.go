package commands

import (
	"fmt"

	"gitlab.com/yakshaving.art/wanginator/internal"
	"gitlab.com/yakshaving.art/wanginator/pkg/defaults"
	"gitlab.com/yakshaving.art/wanginator/pkg/random"
)

// FromConfig creates a list of commands to execute from the configuration
// passed in
func FromConfig(c internal.Config, globalKeep int) []Command {
	write := make([]Command, 0)
	clean := make([]Command, 0)

	for _, t := range c.Tokens {
		write = append(write, CreateAndRegisterToken{
			Username: t.Username,
			Name:     fmt.Sprintf("%s-%s", t.Name, random.Word(16)),

			// Default the scope to api. Apparently defaulting strings in structs
			// doesn't do what it says on the tin.
			Scope: defaults.String(t.Scope, "api"),

			Groups:   t.Groups,
			Projects: t.Projects,
			VarName:  t.Varname,
		})

		keep := globalKeep
		if t.Keep != nil {
			keep = *t.Keep
		}

		clean = append(clean, RevokeTokens{
			Username: t.Username,
			Keep:     keep,
		})
	}
	return append(write, clean...)
}
