# Wanginator

Zarrotrust your gitlab API tokens like a red-team player.

## What does that even mean

Wanginator will load a configuration file in which there is a list of gitlab
users that you want to impersonate through the API.

It will then create an impersonation token for each of these users in the
list, and it will register this token as a group variable in the same
instance.

Once done with all the token creation it will fetch all the existing
impersonation tokens for each user and will revoke active tokens until there
are only 2 left (or whatever is defined as an argument)

### What is this good for

With Wanginator you can feel like a pro skkrty engineer which rotates their
API tokens as frequently as you want (hourly?) to then use these API tokens
in your CI pipelines.

Used together with Hurrdurr you can build a killer skkrty baseline for your
own gitlab instance[s]

We strongly recommend using Wanginator to rotate Wanginator API token itself.
Inception! :amazed:

## Usage

`wanginator <args>`

By default wanginator will look for a wanginator.yml file which contains the
configuration for it to run.

### Environment vars

- #### GITLAB_TOKEN

Required. The token used to talk to the gitlab API, it's recommended to
control the token with Wanginator itself for optimum amazeballs coolness.

- #### GITLAB_BASEURL

Required. The base url to the gitlab instance API.

### Arguments

- #### -debug

Enable debug mode: prints more stuff, and adds timestamps to each line so you
can see how slow your gitlab instance is.

- #### -config wanginator.yml

Config file to read settings from.

- #### -dryrun

Use dryrun mode. It will read stuff, but will not run any. Useful to check
that you are properly configured. It will also tell you what it will write to.

- #### -keep N

How many tokens to keep alive. Old tokens will be removed from older to newer
keeping alive as many as indicated in this argument. (2 by default)

- #### -version

Print the version and quit, so you know which version are you actually using.

### Sample config

```yaml
tokens:
- name: wookiepowah
  username: tricia
  scope: api
  varname: APITOKEN
  groups:
  - skkrty
  projects:
  - testbed/wookienator
```

You can omit `scope` from the config, which will create the token with `api` scope.
