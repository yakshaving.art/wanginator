FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:latest

RUN apk --no-cache add libc6-compat

COPY wanginator /usr/local/bin

CMD [ "/usr/local/bin/wanginator" ]
