package defaults_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/yakshaving.art/wanginator/pkg/defaults"
)

func TestDefaultStrings(t *testing.T) {
	a := require.New(t)
	a.Equal("default_text", defaults.String("", "default_text"))
	a.Equal("non_default", defaults.String("non_default", "default_text"))
}
