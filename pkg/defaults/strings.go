package defaults

func String(s, defaultString string) string {
	if s == "" {
		return defaultString
	}
	return s
}
